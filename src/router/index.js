import Vue from 'vue'
import Router from 'vue-router'

// 登陆
const Login =  r => require.ensure([], () => r(require('../pages/Login')), 'a')
const Main =  r => require.ensure([], () => r(require('../pages/main')), 'b')
// const WantAccess =  r => require.ensure([], () => r(require('../pages/wantAccess')), 'c')

// 权限管理
const addadmin =  r => require.ensure([], () => r(require('../pages/addadmin')), 'i')

// 出入库
const storeOut =  r => require.ensure([], () => r(require('../wareHouse/storeOut')), 'a');
const storeIn  =  r => require.ensure([], () => r(require('../wareHouse/storeIn')),  'b');
// 销量地图
const saleOutMap  =  r => require.ensure([], () => r(require('../pages/saleOutMap')),'c');

/************************************************************************/
Vue.use(Router)

export default new Router({
    routes:[
        // 没有权限
        // {
        //     path:"/WantAccess",
        //     name:"WantAccess",
        //     component:WantAccess,
        // },
        //主界面
        {
            path:"/",
            name:"Main",
            component:Main,
            children:[
                // 添加管理员
                {
                    path:"/addadmin",
                    name:"addadmin",
                    component:addadmin,
                },
                // 入库
                {
                    path:"/storeIn",
                    name:"storeIn",
                    component:storeIn,
                },
                // 出库
                {
                    path:"/storeOut",
                    name:"storeOut",
                    component:storeOut,
                },
                // 销量地图
                {
                    path:"/saleOutMap",
                    name:"saleOutMap",
                    component:saleOutMap,
                },
            ]
        },
        //  登录页面
        {
            path:"/login",
            name:"Login",
            component:Login,
        },
      
    ]
})

