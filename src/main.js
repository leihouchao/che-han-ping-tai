import Vue from 'vue'
import App from './App'
import router from './router'
import $ from 'jquery'
import iView from 'iView'
import VueResource from 'vue-resource';
import 'lib-flexible'
require('iView/dist/styles/iview.css')

Vue.use(VueResource);
Vue.use(iView);
Vue.config.productionTip = false

new Vue({
     el: '#app',
     router,
     template: '<App/>',
     components: {App}
});
